package SeleniumAssignment;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class orangeLocation {

		 public static void main(String[] args) throws InterruptedException, AWTException {
				WebDriver driver = new ChromeDriver();
				
				driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
				
				driver.manage().window().maximize();
				
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
				
				WebElement username = driver.findElement(By.name("username"));
				username.sendKeys("Admin");
				
				WebElement password = driver.findElement(By.name("password"));
				password.sendKeys("admin123");
				
				WebElement login = driver.findElement(By.xpath("//button[text()=' Login ']"));
				login.click();
				
				WebElement admin = driver.findElement(By.xpath("//span[text()='Admin']"));
				admin.click();
				
				WebElement organization = driver.findElement(By.xpath("//span[text()='Organization ']"));
				organization.click();
				
				WebElement location = driver.findElement(By.xpath("//a[text()='Locations']"));
				location.click();
				
				WebElement add = driver.findElement(By.xpath("(//button[@type='button'])[4]"));
				add.click();
				
				WebElement name = driver.findElement(By.xpath("//input[@placeholder='Type here ...']"));
				name.sendKeys("John Kennedy");
				
				WebElement city = driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[2]"));
				city.sendKeys("Florida");
				
				WebElement state = driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[3]"));
				state.sendKeys("Tallahassee");
				
				WebElement postal = driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[4]"));
				postal.sendKeys("32013");
				
				WebElement country = driver.findElement(By.xpath("//div[@class='oxd-select-text--after']"));
				country.click();
				
				Robot robot = new Robot();
				
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.keyRelease(KeyEvent.VK_DOWN);
				
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.keyRelease(KeyEvent.VK_DOWN);
				
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				
				WebElement phone = driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[5]"));
				phone.sendKeys("1234567");
				
				WebElement fax = driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[6]"));
				fax.sendKeys("123456");
				
				WebElement address = driver.findElement(By.xpath("//textarea[@placeholder='Type here ...']"));
				address.sendKeys("Florida,Tallahassee,unitedstates");
				
				WebElement notes = driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[2]"));
				notes.sendKeys("Automated by test engineer");
				
				Thread.sleep(3000);
				
				WebElement save = driver.findElement(By.xpath("//button[@type='submit']"));
				save.click();
				
		}


	}






